import React, { Component } from "react";
import {Route, NavLink} from "react-router-dom";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList";
class App extends Component {
  state = {
    todos: todosList,
    value:""
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value })
  }
    handleCreateTodo = (event) => {
if (event.key === "Enter"){

      const newTodo = {
      userId: 1,
      id: Math.floor (Math.random() * 100000000),
      title: this.state.value,
      completed: false,
    };
    const newTodos = this.state.todos.slice();

    newTodos.push(newTodo)

    this.setState({ todos: newTodos})
    }
  }
  handleDeleteTodo = (todoIdToDelete) => (event) => {

const newTodos = this.state.todos.slice();
const todoIndexToDelete = newTodos.findIndex(todo => {
  if (todo.id === todoIdToDelete) {
    return true;
  }
  return false;
})
newTodos.splice(todoIndexToDelete, 1);
this.setState({todos: newTodos})
  }

  toggleCompleted = (todoIdToToggle) => (event) => {
    const newTodos = this.state.todos.map (toDo => {
      if (toDo.id === todoIdToToggle.id){
        toDo.completed = !toDo.completed
      }
      return toDo;
    })
    this.setState({todos: newTodos})
  }
  handleDeleteSelected = () => {
    const newTodos = this.state.todos.filter (todo => {
      if (todo.completed === true) {
      return false;
    } else {
      return true;
    }})
    this.setState({todos: newTodos});
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route path="/" exact render={()=><TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.state.todos} toggleCompleted = {this.toggleCompleted} />}></Route>
        <Route path="/active" render={()=><TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.state.todos.filter(todo => todo.completed === false)} toggleCompleted = {this.toggleCompleted} />}></Route>
        <Route path="/completed" render={()=><TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.state.todos.filter(todo => todo.completed === true)} toggleCompleted = {this.toggleCompleted} />}></Route>
        <footer className="footer">
  {/* <!-- This should be `0 items left` by default --> */}
  <span className="todo-count">
    <strong>todos={this.state.todos.filter(todo => todo.completed === false).length}</strong> item(s) left
  </span>
  <ul className="filters">
    <li>
      <NavLink exact activeClassName="selected" to="/">All</NavLink>
    </li>
    <li>
      <NavLink activeClassName="selected" to="/active">Active</NavLink>
    </li>
    <li>
      <NavLink activeClassName="selected" to="/completed">Completed</NavLink>
    </li>
  </ul>
  <button className="clear-completed">Clear completed</button>
</footer>
      </section>
    );
  }
}
export default App;
